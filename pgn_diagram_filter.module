<?php
/**
 * Implementation of hook_help().
 */
function pgn_diagram_filter_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/help#pgn_diagram_filter':
      $help_text = '<pre>' . file_get_contents(drupal_get_path('module', 'pgn_diagram_filter') . '/README.txt') . '</pre>
        <p><strong>IMPORTANT</strong>: the !link_ctpgnv requires a visible
          attribution link as a condition for use. But please feel free to
          restyle the one I\'ve provided, or to place it somewhere else.
          Note, however, that I disclaim all responsibility whatsoever for
          damages resulting from non-compliance with the !link_ctpgnvl -
          please be sure you read and understand it before using the viewer.</p>';
      $output = t(
        $help_text,
        array(
          '!link_ctpgnv'   => l('ChessTempo PGN Viewer', 'http://chesstempo.com/pgn-viewer.html'),
          '!link_ctpgnvl'  => l('license terms of the ChessTempo PGN Viewer', 'http://creativecommons.org/licenses/by-nc-nd/2.5/au/'),
        )
      );
      break;
  case 'admin/settings/pgn_diagram_filter':
    $output .= t('<p>Configure default board settings and display
      options below.  All of these options can be overridden for
      individual boards.</p>');
    break;
  }
  return $output;
}

/**
 * Implementation of hook_menu().
 */
function pgn_diagram_filter_menu() {
  $items = array();
  $items['admin/settings/pgn_diagram_filter'] = array(
    'title' => 'PGN Diagram Filter',
    'description' => 'PGN Diagram Filter settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pgn_diagram_filter_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implementation of hook_admin_settings().
 */
function pgn_diagram_filter_admin_settings() {
  $settings = _pgn_diagram_filter_get_settings();
  $form['pgn_diagram_filter'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('PGN Diagram Filter'),
    '#collapsible' => FALSE,
  );
  $form['pgn_diagram_filter']['pgn_diagram_filter_scheme'] = array(
    '#type'          => 'select',
    '#title'         => t('Default colour scheme'),
    '#options'       => drupal_map_assoc(str_replace('-', ' ', $settings['schemes']), 'drupal_ucfirst'),
    '#default_value' => variable_get('pgn_diagram_filter_scheme', 'gray'),
    '#description'   => t('Choose the default colour scheme.'),
  );
  $form['pgn_diagram_filter']['pgn_diagram_filter_piece_set'] = array(
    '#type'          => 'select',
    '#title'         => t('Default piece set'),
    '#options'       => drupal_map_assoc($settings['sets'], 'drupal_ucfirst'),
    '#default_value' => variable_get('pgn_diagram_filter_piece_set', 'merida'),
    '#description'   => t('Choose the default piece set'),
  );
  $form['pgn_diagram_filter']['pgn_diagram_filter_piece_size'] = array(
    '#type'          => 'select',
    '#title'         => t('Default piece size'),
    '#options'       => $settings['sizes'],
    '#default_value' => variable_get('pgn_diagram_filter_piece_size', 29),
    '#description'   => t('Choose the default piece size in nodes bodies.'),
  );
  /* $form['pgn_diagram_filter']['pgn_diagram_filter_block_size'] = array(
    '#type'          => 'select',
    '#title'         => t('Block piece size'),
    '#options'       => $settings['sizes'],
    '#default_value' => variable_get('pgn_diagram_filter_block_size', 20),
    '#description'   => t('Choose the piece size for diagrams displayed in blocks; this always overrides inline SIZE option in blocks.'),
  ); */
  $form['pgn_diagram_filter']['pgn_diagram_filter_coords'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show coordinates.'),
    '#default_value' => variable_get('pgn_diagram_filter_coords', 0),
    '#return_value'  => 1,
    '#description'   => t('Check to show coordinates on the board by default.'),
  );
  /* $form['pgn_diagram_filter']['pgn_diagram_filter_block_moves'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show moves in blocks.'),
    '#default_value' => variable_get('pgn_diagram_filter_block_moves', 0),
    '#return_value'  => 1,
    '#description'   => t('Check to show the move list in blocks by default. If unchecked, inline MOVES options will always be ignored.'),
  ); */
  $form['pgn_diagram_filter']['pgn_diagram_filter_move_list'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show move list.'),
    '#default_value' => variable_get('pgn_diagram_filter_move_list', 0),
    '#return_value'  => 1,
    '#description'   => t('Check to show the move list by default.'),
  );
  $form['pgn_diagram_filter']['pgn_diagram_filter_match_info'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show match information.'),
    '#default_value' => variable_get('pgn_diagram_filter_match_info', 1),
    '#return_value'  => 1,
    '#description'   => t('Check to show match info by default.'),
  );
  $form['pgn_diagram_filter']['pgn_diagram_filter_teaser_display'] = array(
    '#type'          => 'radios',
    '#title'         => t('Teaser Display'),
    '#default_value' => variable_get('pgn_diagram_filter_teaser_display', 0),
    '#options'       => array(0 => t('strip'), 1 => t('prepend miniature board '), 2 => t('prepend text')),
    '#description'   => t('Choose how PGN Diagram Filter tags should be treated in teasers.
                            Note: changing this setting will only apply to newly created nodes/teasers.'),
  );
  return system_settings_form($form);
}

/**
 * Implementation of hook_filter().
 */
function pgn_diagram_filter_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
  case 'no cache':
    return TRUE;
  case 'list':
    return array(0 => t('PGN diagram filter'));
  case 'description':
    return t('Use [pgn:PGN: ... :endpgn] to insert a PGN diagram.
      The PGN may include match information in square brackets ("[", "]"), comments in curly braces ("{", "}").');
  case 'prepare':
    return $text;
  case 'process':
    // Passing arg(0) as $filter allows for contextualization of output.
    $text = _pgn_diagram_filter_replace_tags($text, arg(0));
    return $text;
  case 'settings':
    return pgn_diagram_filter_admin_settings();
  default:
    return $text;
  }
}

/**
 * Implementation of hook_uninstall().
 */
function pgn_diagram_filter_uninstall() {
  $vars = array('scheme', 'piece_set', 'piece_size');
  foreach ($vars as $var) {
    variable_del('pgn_diagram_filter_' . $var);
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function pgn_diagram_filter_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'prepare':
      // Prevent teaser goofiness when updating nodes that have PGN filters.
      if (preg_match('/(\[pgn:.*:endpgn\])/sU', $node->body)) {
        unset($node->teaser);
      }
      break;
    case 'presave':
      // Get user preference for teaser display
      $teaser_display = variable_get('pgn_diagram_filter_teaser_display', 0);
      switch ($teaser_display) {
        case 0:
          // Strip diagrams from teaser.
          $teaser = preg_replace('/\[pgn:.*:endpgn\]/sU', '', $node->body);
          break;
        case 1:
          // Use placeholder image
          $placeholder  = '<img src="' . drupal_get_path('module', 'pgn_diagram_filter');
          $placeholder .= '/img/placeholder.jpg" height="48" width="48" alt="PGN Diagram" class="pgn-prepend-image" />';
          $teaser = $placeholder . preg_replace('/\[pgn:.*:endpgn\]/sU', '', $node->body);
          break;
        case 2:
          // Use placeholder text.
          $placeholder = '<span class="pgn-prepend-text">' . t('PGN Diagram') . '</span> ';
          $teaser = $placeholder . preg_replace('/\[pgn:.*:endpgn\]/sU', '', $node->body);
          break;
      }
      $node->teaser = node_teaser($teaser) . '<!--break-->';
      break;
  }
}

/**
 * Private helper function to get the module settings.
 * @return multidimensional associative  array of module settings
 */

function _pgn_diagram_filter_get_settings() {
  $settings = array(
    'default_scheme'  => variable_get('pgn_diagram_filter_scheme',      'gray'),
    'default_set'     => variable_get('pgn_diagram_filter_piece_set',   'merida'),
    'default_size'    => variable_get('pgn_diagram_filter_piece_size',  29),
    'coords'          => variable_get('pgn_diagram_filter_coords',      0),
    'move_list'       => variable_get('pgn_diagram_filter_move_list',   1),
    //'block_moves'     => variable_get('pgn_diagram_filter_block_moves', 0),
    'match_info'      => variable_get('pgn_diagram_filter_match_info',  1),
    'sets'            => array('merida', 'leipzig', 'maya', 'condal', 'case', 'kingdom', ),
    'schemes'         => array(
        t('blue'),
        t('red'),
        t('green'),
        t('brown'),
        t('orange'),
        t('gray'),
        t('purple'),
        t('bw'),
        t('gray-marble'),
        t('green-marble'),
        t('light-wood'),
        t('wood'),
    ),
    'sizes'           => array(
        20  => t('tiny'),
        24  => t('small'),
        29  => t('normal'),
        35  => t('medium'),
        40  => t('mediumlarge'),
        46  => t('large'),
        55  => t('extralarge'),
        65  => t('xxl'),
        75  => t('insane'),
    ),

  );
  return $settings;
}

/**
 * Private helper function to prepare a PGN string that the ChessTempo
 * PGN Viewer won't choke on.
 */
function _pgn_diagram_filter_prepare_pgn($string) {
  $string = addcslashes($string, "'");
  $string = str_replace(array("\r\n", "\r", "\n", "\t", ), ' ', $string);
  $string = strip_tags($string);
  $string = filter_xss($string);
  return $string;
}

/**
 * Private helper function to extract PGN meta data.
 * @return an associative array of meta data.  Format: tagname => value.
 */
function _pgn_diagram_filter_extract_meta($string) {
  preg_match_all('/(?<=\[)(.*)[\s\t]+\"(.*)\"(?=\])/sU', $string, $data);
  return is_array($data) ? array_combine($data[1], $data[2]) : FALSE;
}

/**
 * Private helper function to assemble a board template.
 * @return board template string.
 */
function _pgn_diagram_filter_assemble_template($pgn = NULL, $title = NULL, $move_list = NULL, $match_data = NULL) {
  $template = '<div id="{diagram_id}-wrapper" class="pgn_diagram_filter-board-wrapper">
    <script type="text/javascript">
    new PgnViewer({
      boardName:       \'{diagram_id}\',
      pieceSet:        \'{piece_set}\',
      pieceSize:       \'{piece_size}\',
      showCoordinates: {coords},
      pgnString:       \'{pgn_string}\'
    });</script>';
  if ($title !== NULL) {
    $template .= '<h3 id="{diagram_id}-boardtitle" class="diagram-title">{board_title}</h3>';
  }
  $template .= '<div id="{diagram_id}-container" class="{scheme} {float} {fen}"></div>';
  if ($move_list) {
    $template .= '
    <div id="{diagram_id}-movelist" class="pgn_diagram_filter-movelist pgn_diagram_filter-collapsible {float}" style="width:{moves_width}px;">
      <div class="movelist-title title"><a href="#">' . t('Moves') . '</a></div>
      <div id="{diagram_id}-moves" class="pgn_diagram_filter-collapse {float}" style="width:{moves_width}px;"></div>
    </div>';
  }
  if ($match_data !== NULL) {
    foreach ($match_data as $tag => $value){
        $match_info .= '<dt class="match-info-tag">' . $tag . ':</dt>' . '<dd class="match-info-value">' . stripcslashes($value) . '</dd>';
      }
    $template .= '
      <div id="{diagram_id}-match-info-wrapper" class="pgn_diagram_filter-info pgn_diagram_filter-collapsible {float}">
        <div class="info-title title"><a href="#">' .  t('Match Info') . '</a></div>
        <div id="{diagram_id}-info" class="pgn_diagram_filter-match-info pgn_diagram_filter-collapse">
          <dl id="{diagram_id}-match-info" class="match-info">' .  $match_info .'</dl></div>
      </div>';
  }
  $template .= '</div>';
  return $template;
}

/**
 * Private helper function provide attribution string.
 */
function _pgn_diagram_filter_attribution() {
  return '<div id="chesstempo-attribution">' .
      t('The PGN diagram/s displayed on this page use/s the <a href="@chesstempo">ChessTempo PGN Viewer</a>.',
        array('@chesstempo' => url('http://chesstempo.com/pgn-viewer.html'))
      ) . '</div>';
}

/**
 * Private helper function to replace [pgn:PGN: ... :endpgn] tags with
 *  a ChessTempo PGN Viewer diagram.
 * @param $str - input text that will be processed
 * @param $filter - modify output in some contexts
 * @return processed input text
 */
function _pgn_diagram_filter_replace_tags($str, $filter = NULL) {
  // Set up shop.
  $has_diagram = 0;
  $settings    = _pgn_diagram_filter_get_settings();
  $sizes       = array_flip($settings['sizes']);
  // Examine $filter, to see if we need to do anything special.
  switch ($filter) {
    case 'search':
      // This is a search item, strip out all PGN tags and return.
      return preg_replace('/(\[pgn:.*:endpgn\])/sU', '', $str);
    case 'node':
      if (is_numeric(arg(1))) {
        // Make the node id available for later functionality.
        $nid = arg(1);
      }
      break;
  }
  // Find all PGN diagram filter tags and iterate over them,
  // filling out a diagram template for each one.
  $exploded = preg_split('/(\[pgn:|:endpgn\])/', $str);
  foreach ($exploded as $index => $fragment) {
    // There's no need to do anything more if there isn't a PGN or a file
    // indicated in the tag.
    if (strstr($fragment, 'PGN:') || strstr($fragment, 'FILE:')) {
      // Reset settings and strings to defaults.
      $piece_set   = $settings['default_set'];
      $piece_size  = $settings['default_size'];
      $scheme      = $settings['default_scheme'];
      $movelist    = $settings['move_list'];
      $meta        = $settings['match_info'];
      $coords      = $settings['coords'];
      $diagram_id  = 'pgn-diagram-' . uniqid();
      $float       = 'left';
      $board_title = NULL;
      $metadata    = NULL;
      $match_data  = NULL;
      $fen         = '';
      // Explode the data within the PGN diagram filter tag and iterate over it, setting options as requested.
      $chunks = explode('|', $fragment);
      foreach ($chunks as $chunk_index => $chunk) {
        if (preg_match('/^[A-Z]*:/', $chunk)) {
          $data = preg_split('/(PGN|FILE|INFO|ALIGN|PIECES|SCHEME|SIZE|COORDS|MOVES|TITLE):/', $chunk, NULL, PREG_SPLIT_DELIM_CAPTURE);
          switch ($data[1]) {
            case 'FILE':
              // Construct file name.  Files must be located in the pgn/
              // subdirectory of the default files directory (usually sites/default/files).
              $filename = file_directory_path() . '/pgn/' . check_plain($data[2]);
              // load the file into $pgn_string if it exists
              if (preg_match('/.*\.pgn$/', $filename) && file_exists($filename)) {
                $pgn_string = _pgn_diagram_filter_prepare_pgn(file_get_contents($filename));
              }
              break;
            case 'PGN':
              // If an attachment is indicated, load it's contents in
              // place of $data[2].
              if (preg_match('/attachment#([0-9])+/', $data[2], $attachment)) {
                $nid && $node = node_load($nid);
                // There can be any number of attachments.  Find out which
                // one they want.
                while ($attachment[1] >= 0) {
                  $file = array_shift($node->files);
                  $attachment[1]--;
                }
                // Load the file into $pgn_string if it exists
                $pgn_string = (preg_match('/.*\.pgn$/', $file->filename) && file_exists($file->filepath))
                  ? file_get_contents($file->filepath)
                  : "file not accessible";
              }
              else {
                $pgn_string = $data[2];
              }
              $pgn_string = _pgn_diagram_filter_prepare_pgn($pgn_string);
              // If this is a FEN without moves, hide the move buttons
              // by adding the fen class.
              if (strstr($pgn_string, 'FEN') && !strstr($pgn_string, '1.')) {
                  $fen = ' fen';
              }
              break;
            // Lowercase and trim the passed value for ALL the next options,
            // just in case the user was careless.
            $data[2] = drupal_strtolower(trim($data[2]));
            case 'ALIGN':
              $float = check_plain($data[2]);
              break;
            case 'PIECES':
              in_array($data[2], $settings['sets']) && $piece_set = $data[2];
              break;
            case 'SCHEME':
              if (in_array($data[2], $settings['schemes'])) {
                $has_scheme = TRUE;
                $scheme     = $data[2];
              }
              break;
            case 'SIZE':
              array_key_exists($data[2], $sizes) && $piece_size = $sizes[$data[2]];
              break;
            case 'COORDS':
              $coords = filter_xss($data[2]);
              break;
            case 'MOVES':
              $movelist = $data[2];
              break;
            case 'INFO':
              $meta = $data[2];
              break;
            case 'TITLE':
              $board_title = check_plain(strip_tags($data[2]));
              break;
          }
        }
      }
      // Always suppress the move list if there aren't any moves.
      !preg_match('/1\./', $pgn_string) && $movelist = 0;
      // Add match data, if indicated.
      $meta && $match_data = _pgn_diagram_filter_extract_meta($pgn_string);
      // Insert values into the diagram template.
      $exploded[$index] = str_replace(
        array(
          '{board_title}', '{diagram_id}', '{pgn_string}', '{moves_width}', '{piece_set}', '{piece_size}', '{scheme}', '{float}', '{fen}', '{coords}', ), array(
           $board_title,    $diagram_id,    $pgn_string,  ($piece_size * 8), $piece_set,    $piece_size,    $scheme,    $float,    $fen,    $coords,
        ),
        _pgn_diagram_filter_assemble_template($pgn_string, $board_title, $movelist, $match_data)
      );
      // Increment $has_diagram (we won't render anything if we haven't
      // ever made it this far).
      $has_diagram++;
    }
  }
  if ($has_diagram) {
    // Set filter run switch and add remote and local JS and CSS, if
    // this hasn't been done already.
    if (!defined('PGN_DIAGRAM_FILTER_RUN_SWITCH')) {
      define('PGN_DIAGRAM_FILTER_RUN_SWITCH', TRUE);
      drupal_set_html_head('    <script type="text/javascript" src="http://chesstempo.com/js/pgnyui.js"></script>
      <script type="text/javascript" src="http://chesstempo.com/js/pgnviewer.js"></script>
      <link type="text/css" rel="stylesheet" href="http://chesstempo.com/css/board-min.css"></link>');
      // Add own jQuery JS for collapsing the moves list and match info boxes.
      drupal_add_js(drupal_get_path('module', 'pgn_diagram_filter') . '/js/pgnDiagramFilterCollapse.js');
    }
    // Set output to processed text + attribution.
    $str = implode($exploded) . _pgn_diagram_filter_attribution();
  }
  // Always add module CSS, because we need it for blocks, teasers, and
  // search results, too.
  drupal_add_css(drupal_get_path('module', 'pgn_diagram_filter') . '/css/pgn_diagram_filter_styles.css');
  return $str;
}
