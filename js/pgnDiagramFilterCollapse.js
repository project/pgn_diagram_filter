$(document).ready(function(){
  $(".pgn_diagram_filter-collapse").hide();
  $(".pgn_diagram_filter-collapsible .title a").toggle(
    function() {
      $(this).parent().next(".pgn_diagram_filter-collapse").slideDown("medium");
      return false;
    },
    function() {
      $(this).parent().next(".pgn_diagram_filter-collapse").slideUp("medium");
      return false;
    }
  );
});
