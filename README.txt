This module allows the insertion of PGNs (ie., chess games, or snippets)
into nodes or blocks.

USAGE
=====
1. Install and activate the module.  See
    http://drupal.org/getting-started/install-contrib/modules if you
    don't know how to do this.

2. If you like, choose different defaults for piece set, size, colour
    scheme, and coordinate and moves list display.
    See admin/settings/pgn_diagram_filter

3. Add the "PGN Diagram Filter" to the Input Format of your choice.
    See admin/settings/filters

4. Create a node or block, making sure that the chosen input filter is
    selected for that node or block.

5. Input the body of your node or block, appending "[pgn:" and "PGN:"
    before your PGN data, and ":endpgn]" after it (without quotes).  The
    PGN may contain match information enclosed in square brackets, as
    well as annotations contained in curly braces.
      HINT #1: You can upload a file to sites/default/files/pgn/ via
        FTP, and load it into the viewer.  Use the FILE option to do
        this.  Using this feature will cause any PGN you include to be
        ignored.
      HINT #2: If "PGN:" is followed by nothing or simply spaces, a
        blank board will be displayed, though inserting any non-PGN text
        will cause a standard initial position to be displayed.
      HINT #3: Insert a static position with: [FEN "yourFENhere"]
        - replace yourFENhere with a valid FEN, of course - but the
        quotes are required.  Move buttons for these are automatically
        turned off, if no moves are given.

6. You can specify a PGN file to be loaded (previously uploaded via FTP)
    different piece sets and sizes, colour schemes, board alignment, and
    coordinate, match information, and move list visibility, and give
    the board a title, by adding the option name in caps followed by a
    colon, and the option setting.  Valid option names are:
      ALIGN, COORDS, FILE, INFO, MOVES, PIECES, SCHEME, MOVES, and TITLE.

   The currently supported options settings are as follows:
      ALIGN: right (default is left; ignored in blocks)
     COORDS: 0 (off; default) or 1 (on)
       FILE: (pathless) filename of a PGN located in
              sites/default/files/pgn/
       INFO: 0 (off) or 1 (on; default)
      MOVES: 0 (off) or 1 (on; default)
     PIECES: merida (default), leipzig, maya, condal, case, or kingdom
     SCHEME: red, green, blue, brown, orange, gray (default), bw,
              green-marble, gray-marble, wood, light-wood
       SIZE: tiny, small, normal (default), medium, mediumlarge, large,
              extralarge, xxl, insane
      TITLE: any plain text (*all* HTML and JavaScript are removed)
  Values other than these are ignored.

  Options may be specified in any order, and may be placed before or
  after "PGN:yourPGN", but all options must be separated by a single
  pipe (ie., | ), improperly capitalized option names will not be
  recognized, but option settings are not case sensitive.

7. PGNs in file attachments uploaded via the core Upload module can be
   loaded with the following syntax: [pgn:PGN:attachment#x:endpgn] where
   x indicates which attachment to try to load, 0 being the first.
